<?php

namespace App\Controller;

use App\Entity\Requester;
use App\Repository\RequesterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ResumeController extends Controller
{
    /**
     * @Route("", name="resume")
     *
     * @return Response
     */
    public function index()
    {
        return $this->render('resume/index.html.twig', []);
    }

    /**
     * @Route("/references", name="request_references")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param RequesterRepository $requesterRepository
     * @return JsonResponse
     */
    public function requestReferences(Request $request, EntityManagerInterface $em, RequesterRepository $requesterRepository, string $console)
    {
        $email = $request->request->get('email', null);
        $name = $request->request->get('name', null);
        $company = $request->request->get('company', null);

        if ( ! $email || ! $name || ! $company ) {
            return new JsonResponse([
                'success'   => false,
                'message'   => 'Required Parameter Missing'
            ]);
        }

        if ( filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {
            return new JsonResponse([
                'success'   => false,
                'message'   => 'That is not a valid e-mail'
            ]);
        }

        if ( strlen($name) < 4 || strlen($name) > 254 ) {
            return new JsonResponse([
                'success'   => false,
                'message'   => 'Not a valid name.  Must be between 4 and 254 characters'
            ]);
        }

        if ( strlen($company) < 3 || strlen($company) > 254 ) {
            return new JsonResponse([
                'success'   => false,
                'message'   => 'Not a valid company name.  Must be between 3 and 254 characters'
            ]);
        }

        /** @var Requester $existing */
        if ( $existing = $requesterRepository->findOneBy(['email' => $email]) ) {
            $command = $console . ' ' . $existing->getId() . ' false';

            $process = new Process($command);
            $process->run();

            return new JsonResponse([
                'success'   => false,
                'message'   => "You've already requested references!  I'll get back to you shortly."
            ]);
        }

        $requester = new Requester($email, $name, $company);
        $em->persist($requester);
        $em->flush();

        $command = $console . ' ' . $requester->getId() . ' true';

        $process = new Process($command);
        $process->run();

        return new JsonResponse([
            'success'   => true,
            'message'   => "Thanks for your interest!  I'll get back to you shortly."
        ]);
    }
}
