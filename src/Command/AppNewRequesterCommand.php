<?php

namespace App\Command;

use App\Repository\RequesterRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppNewRequesterCommand extends Command
{
    /**
     * @var RequesterRepository
     */
    private $requesterRepository;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(RequesterRepository $requesterRepository, \Swift_Mailer $mailer)
    {
        parent::__construct();
        $this->requesterRepository = $requesterRepository;
        $this->mailer = $mailer;
    }

    protected static $defaultName = 'app:new-requester';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('requester', InputArgument::REQUIRED, 'Requester ID')
            ->addArgument('new', InputArgument::OPTIONAL, 'Is New')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $requesterId = $input->getArgument('requester');
        $new = $input->getArgument('new');

        $requester = $this->requesterRepository->find($requesterId);

        if ( ! $requester ) {
            return;
        }

        $email = $requester->getEmail();
        $name = $requester->getName();
        $company = $requester->getCompany();
        $reRequest = $new === 'true' ? 'yes' : 'no';

        $message = (new \Swift_Message("New References Request"))
            ->setFrom('noreply@gdibass.com')
            ->setTo('matt@gdibass.com')
            ->setBody(<<<EOF
**Email** {$email} \r\n
**Name** {$name} \r\n
**Company** {$company} \r\n
**New** {$reRequest} \r\n
EOF
,
                'text/html'
            );

        $this->mailer->send($message);
    }
}
