<?php

namespace App\Repository;

use App\Entity\Requester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Requester|null find($id, $lockMode = null, $lockVersion = null)
 * @method Requester|null findOneBy(array $criteria, array $orderBy = null)
 * @method Requester[]    findAll()
 * @method Requester[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequesterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Requester::class);
    }

//    /**
//     * @return Requester[] Returns an array of Requester objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Requester
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
