'use strict';

import '../scss/resume.scss';
import 'font-awesome/css/font-awesome.css';
import 'font-awesome-animation/dist/font-awesome-animation.css';
import $ from 'jquery';
import References from './Components/References';


$(document).ready(() => {
    let $references = $('#get-references');
    new References($references);
});
