'use strict';

import $ from 'jquery';
import swal from 'sweetalert2';

class References {
    constructor($link) {
        this.$link = $link;

        this.$link.click(
            'click',
            this.handleClickGetReferences
        );

        this.handleClickGetReferences = this.handleClickGetReferences.bind(this);
    }

    handleClickGetReferences() {
        swal.close();
        swal.mixin({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
        }).queue([
            'Your Name',
            'Your Email',
            'Your Company'
        ]).then((result) => {
            if (result.value) {
                let [name, email, company] = result.value;

                if ( name === '' || email === '' || company === '' ) {
                    swal({
                        type: 'error',
                        title: "You forgot something",
                        html: "All 3 inputs are required"
                    });
                } else {
                    swal({
                        title: 'Sending Request',
                        showConfirmButton: false,
                        html: '<i class="fa fa-th-large fa-5 fa-spinner faa-spin animated" style="font-size:100px;"></i>'
                    });
                    $.ajax({
                        url: '/references',
                        method: 'POST',
                        data: 'name=' + name + '&email=' + email + '&company=' + company
                    }).then((response) => {
                        swal({
                            type: response.success ? 'success' : 'error',
                            html: response.message ? response.message : response.success ? 'Got your request.  I will get back to you!' : 'Oops, something went wrong'
                        });
                    }).catch(() => {
                        swal({
                            type: 'error',
                            html: 'Something went wrong!'
                        });
                    });
                }
            }
        })
    }
}

export default References;